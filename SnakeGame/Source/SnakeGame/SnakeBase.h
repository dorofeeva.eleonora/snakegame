// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class APlayerPawnBase;
class AFood;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	int FloorSize;

	UPROPERTY(EditDefaultsOnly)
	int StartSnakeSize;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY()
	APlayerPawnBase* Pawn;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BluePrintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BluePrintCallable)
	void IncreaseSnakeSpeed(float koef = 0.9f);
	UFUNCTION(BluePrintCallable)
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement
		, AActor* Other);

	/** Accessor to inital stamina	*/
	UFUNCTION(BlueprintPure, Category = "Stamina")
	int GetInitialStamina() { return InitialScore; }

	/** Accessor to current stamina	*/
	UFUNCTION(BlueprintPure, Category = "Score")
	int GetCurrentScore() { return CurrentScore; }

	/** Update current stamina
		@param Score player points for eating food	*/
	UFUNCTION(BlueprintCallable, Category = "Score")
	void UpdateCurrentScore(int Score = 1) { CurrentScore += Score; }

	UFUNCTION()
	void RestartGame();

	UFUNCTION()
	bool GetNewLocation(FVector& Location);

	UFUNCTION()
	bool LocationIsColliding(FVector& Location);
private:
	UPROPERTY(EditAnywhere, Category = "Score")
	int InitialScore;

	UPROPERTY(EditAnywhere, Category = "Score")
	int CurrentScore;

	UPROPERTY()
	int MaxSnakeLength;

	UPROPERTY()
	int MinSnakeLength;
};
