// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"
#include "Food.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	LastMoveDirection = EMovementDirection::UP;
	MovementSpeed = 10.0f;
	InitialScore = 0;
	CurrentScore = InitialScore;
	MinSnakeLength = 4;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	MaxSnakeLength = ((FloorSize * 2 - ElementSize * 2) / ElementSize) * ((FloorSize * 2 - ElementSize * 2) / ElementSize);
	
	if (StartSnakeSize > MaxSnakeLength)
		StartSnakeSize = MaxSnakeLength;
	else if (StartSnakeSize < MinSnakeLength)
		StartSnakeSize = MinSnakeLength;

	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(StartSnakeSize);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	FVector Location;
	if (GetNewLocation(Location))
		GetWorld()->SpawnActor<AFood>(FoodClass, Location, FRotator(0,0,0), SpawnParameters);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	//���� ������ �������� �� ���� - ��������� ����� �������� ������ - ����� �������
	
	if (SnakeElements.Num() == MaxSnakeLength)
	{
		RestartGame();
	}
	else
	{
		for (int i = 0; i < ElementsNum; ++i)
		{
			FVector NewLocation(ForceInitToZero);
			if (SnakeElements.Num() > 0)
			{
				NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
				if (SnakeElements.Num() == 1)
				{
					float Delta = SnakeElements.Num() * ElementSize;
					switch (LastMoveDirection)
					{
					case EMovementDirection::UP:
						NewLocation.X -= Delta;
						break;
					case EMovementDirection::DOWN:
						NewLocation.X += Delta;
						break;
					case EMovementDirection::LEFT:
						NewLocation.Y -= Delta;
						break;
					case EMovementDirection::RIGHT:
						NewLocation.Y += Delta;
						break;
					}
				}
				else
				{
					FVector Delta = SnakeElements[SnakeElements.Num() - 2]->GetActorLocation() - NewLocation;
					NewLocation += Delta;
				}
			}

			FTransform NewTransform = FTransform(NewLocation);
			ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
			NewSnakeElem->SnakeOwner = this;
			int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
			if (ElementIndex == 0)
			{
				NewSnakeElem->SetFirstElementType();
			}
		}
	}
}

void ASnakeBase::IncreaseSnakeSpeed(float koef)
{
	MovementSpeed *= koef;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	if (SnakeElements.Num() > 0)
	{
		SnakeElements[0]->SetCollision(false);
			for (int i = SnakeElements.Num() - 1; i > 0; i--)
			{
				auto CurrentElement = SnakeElements[i];
				auto PrevElement = SnakeElements[i - 1];
				FVector PrevLocation = PrevElement->GetActorLocation();
				CurrentElement->SetActorLocation(PrevLocation);
			}

		SnakeElements[0]->AddActorWorldOffset(MovementVector);
		SnakeElements[0]->SetCollision(true);
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex = -1;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirstElement = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirstElement);
		}
		else
		{
			RestartGame();
		}
	}
}

void ASnakeBase::RestartGame()
{
	if (IsValid(Pawn))
		Pawn->OnRestart();
}

bool ASnakeBase::GetNewLocation(FVector& Location)
{
	Location.Z = 0.0f;
	//���� ������ �� ��������� �� ���� - ���� ����� ��� ����� ���
	for (int i = 0; i < SnakeElements.Num(); ++i)
	{
		Location.X = FMath::RandRange((-FloorSize + ElementSize) / ElementSize, (FloorSize - ElementSize) / ElementSize) * ElementSize;
		Location.Y = FMath::RandRange((-FloorSize + ElementSize) / ElementSize, (FloorSize - ElementSize) / ElementSize) * ElementSize;
		if (!LocationIsColliding(Location))
			return true;
	}

	return false;
}

bool ASnakeBase::LocationIsColliding(FVector& Location)
{
	for (auto Element : SnakeElements)
	{
		auto box = Element->CalculateComponentsBoundingBoxInLocalSpace();
		box = box.ExpandBy(ElementSize / 2);
		if (box.IsInside(Location))
			return true;
	}
	return false;
}