// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "SnakeBase.h"
#include "SnakeGameGameModeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	bIsPaused = false;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

void APlayerPawnBase::OnRestart()
{
	UGameplayStatics::OpenLevel(GetWorld(), FName(*GetWorld()->GetName()), false);
}

void APlayerPawnBase::OnPausePlay()
{
	bIsPaused = !bIsPaused;
	UGameplayStatics::SetGamePaused(GetWorld(), bIsPaused);
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

	auto toggle = &PlayerInputComponent->BindAction("Start", IE_Pressed, this, &APlayerPawnBase::OnRestart);
	toggle->bExecuteWhenPaused = true; //EVEN THOUGH THE GAME IS PAUSED, CATCH THIS EVENT !!!!

	toggle = &PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &APlayerPawnBase::OnPausePlay);
	toggle->bExecuteWhenPaused = true; //EVEN THOUGH THE GAME IS PAUSED, CATCH THIS EVENT !!!!
	}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->Pawn = this;
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if ((value > 0) && (SnakeActor->LastMoveDirection != EMovementDirection::DOWN))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if ((value < 0) && (SnakeActor->LastMoveDirection != EMovementDirection::UP))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if ((value > 0) && (SnakeActor->LastMoveDirection != EMovementDirection::LEFT))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
		else if ((value < 0) && (SnakeActor->LastMoveDirection != EMovementDirection::RIGHT))
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
	}
}